package br.ifsp.guarucar.hibernate;

import java.util.List;

import br.ifsp.guarucar.classes.*;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.sql.JoinType;

import javassist.runtime.Inner;

@SuppressWarnings("unused")
public class HibernateUtil {

	// configuracao padrao hibernate
	private static final SessionFactory sessionFactory = buildSessionFactory();

	private static SessionFactory buildSessionFactory() {

		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
		serviceRegistryBuilder.applySettings(configuration.getProperties());
		ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
		return configuration.buildSessionFactory(serviceRegistry);
	}

	public static SessionFactory getSessionfactory() {
		return sessionFactory;
	}

	// funcoes do hibernate

	/* essa funcao permite salvar os dados direto no banco de dados */
	public static int salvar(Object object) {
		Session session = getSessionfactory().openSession();
		session.beginTransaction();
		session.save(object);
		session.getTransaction().commit();
		session.close();
		return 0;
	}	

	/* essa função permite deletar algum dado salvo no banco */
	public static void deletar(Object object) {
		Session session = getSessionfactory().openSession();
		session.beginTransaction();
		session.delete(object);
		session.getTransaction().commit();
		session.close();
	}
	
	public static Object pesquisaObjeto(String campo, int valor, Object object) {
		Session session = getSessionfactory().openSession();
		
		Object object2 = new Object();
		object2 = session.createCriteria(object.getClass()).add(Restrictions.eq(campo, valor))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).uniqueResult();
		session.close();
		return object2;
	}
	
	public static List pesquisaIntUnico(String campo, int valor, Object object) {
		Session session = getSessionfactory().openSession();
		List lista = session.createCriteria(object.getClass()).add(Restrictions.eq(campo, valor))
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
		session.close();
		return lista;
	}

	/* fun��o que capturas os dados e os modifica no banco de dados */
	public static void alterar(Object object) {
		Session session = getSessionfactory().openSession();
		session.beginTransaction();
		session.update(object);
		session.getTransaction().commit();
		session.close();
	}
	
	// busca tudo
		@SuppressWarnings("rawtypes")
		public static List buscar(Object object, String ordem) {
			Session session = getSessionfactory().openSession();
			List lista = session.createCriteria(object.getClass()).addOrder(Order.desc(ordem)).list();
			session.close();
			return lista;
		}
		
	//busca com restrições
				@SuppressWarnings("rawtypes")
		public static List buscarStringVeiculo(String campo, String valor) {
			Object object = new Veiculo();
			Session session = getSessionfactory().openSession();
			List lista = session.createCriteria(object.getClass()).add(Restrictions.eq(campo, valor)).list();
			session.close();
			return lista;
		}
				
				@SuppressWarnings("rawtypes")
		public static List buscarAluguel(int valor) {
			Session session = getSessionfactory().openSession();
			List lista = session.createCriteria(Aluguel.class).add(Restrictions.eq("statusEntrega",valor)).list();
			session.close();
			return lista;
		}		
		
	/* essa função responsável por pesquisar o login e senha direto no banco */
	public static Login login(Login login) {
		Session session = getSessionfactory().openSession();
		return (Login) session.createCriteria(Login.class).add(Restrictions.eq("login", login.getLogin()))
				.add(Restrictions.eq("senha", login.getSenha())).uniqueResult();
	}

	/* essa função responsável por pesquisar o login e senha direto no banco 
	public static Usuario login(Usuario usuario) {
		Session session = getSessionfactory().openSession();
		return (Usuario) session.createCriteria(Usuario.class).add(Restrictions.eq("login", usuario.getLogin()))
				.add(Restrictions.eq("senha", usuario.getSenha())).uniqueResult();
	}
	*/

}