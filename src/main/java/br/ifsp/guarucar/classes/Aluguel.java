package br.ifsp.guarucar.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//declaração de que esta classe é uma tabela no banco
@Entity
@Table(name="aluguel")
//declaração dos intens da tabela no banco
public class Aluguel {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAluguel;
	
	private int idCliente;
	
	private int idMotoristaRetirada;
	private int idMotoristaEntrega;
	
	private String placa;
	
	private String ufRetirada;
	private String cidadeRetirada;
	private String enderecoRetirada;
	private String referenciaRetirada;
	
	private String ufEntrega;
	private String cidadeEntrega;
	private String enderecoEntrega;
	private String referenciaEntrega;

	private String dataRetirada;
	private String dataEntrega;
	private int statusRetirada;
	private int statusEntrega;
	
	public int getIdAluguel() {
		return idAluguel;
	}
	public void setIdAluguel(int idAluguel) {
		this.idAluguel = idAluguel;
	}
	
	
	
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public int getIdMotoristaRetirada() {
		return idMotoristaRetirada;
	}
	public void setIdMotoristaRetirada(int idMotoristaRetirada) {
		this.idMotoristaRetirada = idMotoristaRetirada;
	}
	public int getIdMotoristaEntrega() {
		return idMotoristaEntrega;
	}
	public void setIdMotoristaEntrega(int idMotoristaEntrega) {
		this.idMotoristaEntrega = idMotoristaEntrega;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getUfRetirada() {
		return ufRetirada;
	}
	public void setUfRetirada(String ufRetirada) {
		this.ufRetirada = ufRetirada;
	}
	public String getCidadeRetirada() {
		return cidadeRetirada;
	}
	public void setCidadeRetirada(String cidadeRetirada) {
		this.cidadeRetirada = cidadeRetirada;
	}
	public String getEnderecoRetirada() {
		return enderecoRetirada;
	}
	public void setEnderecoRetirada(String enderecoRetirada) {
		this.enderecoRetirada = enderecoRetirada;
	}
	public String getReferenciaRetirada() {
		return referenciaRetirada;
	}
	public void setReferenciaRetirada(String referenciaRetirada) {
		this.referenciaRetirada = referenciaRetirada;
	}
	public String getUfEntrega() {
		return ufEntrega;
	}
	public void setUfEntrega(String ufEntrega) {
		this.ufEntrega = ufEntrega;
	}
	public String getCidadeEntrega() {
		return cidadeEntrega;
	}
	public void setCidadeEntrega(String cidadeEntrega) {
		this.cidadeEntrega = cidadeEntrega;
	}
	public String getEnderecoEntrega() {
		return enderecoEntrega;
	}
	public void setEnderecoEntrega(String enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}
	public String getReferenciaEntrega() {
		return referenciaEntrega;
	}
	public void setReferenciaEntrega(String referenciaEntrega) {
		this.referenciaEntrega = referenciaEntrega;
	}
	public String getDataRetirada() {
		return dataRetirada;
	}
	public void setDataRetirada(String dataRetirada) {
		this.dataRetirada = dataRetirada;
	}
	public String getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(String dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
	public int getStatusRetirada() {
		return statusRetirada;
	}
	public void setStatusRetirada(int statusRetirada) {
		this.statusRetirada = statusRetirada;
	}
	public int getStatusEntrega() {
		return statusEntrega;
	}
	public void setStatusEntrega(int statusEntrega) {
		this.statusEntrega = statusEntrega;
	}
	
	
	
	
		
}
