package br.ifsp.guarucar.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//declaração de que esta classe é uma tabela no banco
@Entity
@Table(name="CorVeiculo")
//declaração dos intens da tabela no banco
public class CorVeiculo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idcor;
	private String nome;
	private int status;
	
	public int getIdcor() {
		return idcor;
	}
	public void setIdcor(int idcor) {
		this.idcor = idcor;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}