package br.ifsp.guarucar.classes;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SuppressWarnings("serial")
@Named("sessao")
@SessionScoped
public class Sessao implements Serializable {
	private Login logado;

	public void login(Login login) {
		this.logado = login;
	}

	public int getTipo() {
		return logado.getIdTipoUsuario();
	}

	public String getNome() {
		return logado.getLogin();
	}
	
	public int getIdLogin() {
		return logado.getIdLogin();
	}

	public boolean isLogado() {
		return logado != null;
	}

	public void logout() {
		this.logado = null;
	}
}
