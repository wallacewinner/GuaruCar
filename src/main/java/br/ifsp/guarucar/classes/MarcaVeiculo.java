package br.ifsp.guarucar.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//declaração de que esta classe é uma tabela no banco
@Entity
@Table(name="MarcaVeiculo")
//declaração dos intens da tabela no banco
public class MarcaVeiculo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idMarcaVeiculo;
	private int nome;
		
}