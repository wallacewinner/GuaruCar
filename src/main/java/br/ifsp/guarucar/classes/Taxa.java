package br.ifsp.guarucar.classes;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//declaração de que esta classe é uma tabela no banco
@Entity
@Table(name="taxa")
//declaração dos intens da tabela no banco
public class Taxa {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idTaxa;
	private int idAluguel;
	private String nome;
	private Float valor;	
	@Column(name="status", columnDefinition = "int default 1") 
	private int status;
	
	
	//get and set
	public int getIdtaxa() {
		return idTaxa;
	}
	public void setIdtaxa(int idtaxa) {
		this.idTaxa = idtaxa;
	}
	public Float getValor() {
		return valor;
	}
	public void setValor(Float valor) {
		this.valor = valor;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdAluguel() {
		return idAluguel;
	}
	public void setIdAluguel(int idAluguel) {
		this.idAluguel = idAluguel;
	}
		
}
