package br.ifsp.guarucar.classes;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


//declaração de que esta classe é uma tabela no banco
@Entity
@Table(name = "Ocorrencia")
// declaração dos intens da tabela no banco
public class Ocorrencia {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idOcorrencia;
	private int idAluguel;
	private int idusuario;
	private String UsuarioNome;
	private String data;
	private String ocorrencia;
	
	public int getIdOcorrencia() {
		return idOcorrencia;
	}
	public void setIdOcorrencia(int idOcorrencia) {
		this.idOcorrencia = idOcorrencia;
	}
	public int getIdAluguel() {
		return idAluguel;
	}
	public void setIdAluguel(int idAluguel) {
		this.idAluguel = idAluguel;
	}
	public String getOcorrencia() {
		return ocorrencia;
	}
	public void setOcorrencia(String ocorrencia) {
		this.ocorrencia = ocorrencia;
	}
	public String getData() {
		return data;
	}
	public void setData(String string) {
		this.data = string;
	}
	public int getIdusuario() {
		return idusuario;
	}
	public void setIdusuario(int idusuario) {
		this.idusuario = idusuario;
	}
	public String getUsuarioNome() {
		return UsuarioNome;
	}
	public void setUsuarioNome(String usuarioNome) {
		UsuarioNome = usuarioNome;
	}

	

}