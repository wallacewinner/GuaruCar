package br.ifsp.guarucar.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.ifsp.guarucar.classes.Sessao;
import br.ifsp.guarucar.classes.Aluguel;
import br.ifsp.guarucar.classes.Login;
import br.ifsp.guarucar.classes.Usuario;
import br.ifsp.guarucar.classes.Veiculo;
import br.ifsp.guarucar.hibernate.HibernateUtil;

@Controller
public class LoginController {
	private final Validator validation;
	private final Sessao sessao;

	/**
	 * @deprecated CDI eyes only
	 */

	protected LoginController() {
		this(null, null);
	}

	@Inject
	public LoginController(Validator validation, Sessao sessao) {
		this.validation = validation;
		this.sessao = sessao;
	}

	@Inject
	private Result result;

	public void novo() {
		// função respondavel pelo redirecionamento para a tela de cadastro
	}

	public void salvar(Login login) {

		// erro
		validation.onErrorRedirectTo(this).novo();

		// salvar
		HibernateUtil.salvar(login);

		// sucesso
		result.include("mensagem", "Usuário cadastrado com sucesso !");
		result.redirectTo(AluguelController.class).inicio(null, null);// redireciona

	}

	// redireciona para pagina de login
	// @Patch ("/")
	public void acesso() {
		if (sessao.isLogado() == true)
			result.redirectTo(AluguelController.class).inicio(null, null);
	}

	// capta os dados do formulário e trata as mesma para redirecionar para a
	// pagina de destino
	public void login(Login login) {
		Login acesso = HibernateUtil.login(login);
		if (acesso == null)
			validation.add(
					new SimpleMessage("erro", "Algo de errado não está certo com seu login, vamos tentar novamente ?"));
		else {
			System.out.println("ok");
			sessao.login(acesso);
			result.redirectTo(AluguelController.class).inicio(null, null);
		}
		validation.onErrorRedirectTo(this).acesso();
	}

	// atualizar dados
	public void atualizardados() {
		// este trecho é responsavel por redirecionar a pagina para que o
		// suuário
		// possa atualizar suas informações pessoais.
		Login login = new Login();
		List<?> resultado = HibernateUtil.pesquisaIntUnico("id", sessao.getIdLogin(), login);

		if (resultado == null) {
			validation.add(new SimpleMessage("erro", "Algo aconteceu, chame o desenvolvedor por favor..."));
		}
		validation.onErrorForwardTo(this).novo();
		result.include("resultado", resultado);
	}

	// sair sessao
	public void logout() {
		sessao.logout();
		result.redirectTo(this).acesso();
	}

	public void alterar(Login login) {
		// erro
		validation.onErrorRedirectTo(AluguelController.class).inicio(null, null);

		// salvar
		HibernateUtil.alterar(login);

		// sucesso
		result.include("mensagem", "Usuário cadastrado com sucesso !");
		result.redirectTo(AluguelController.class).inicio(null, null);// redireciona

	}

}