package br.ifsp.guarucar.controllers;

import java.text.ParseException;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.ifsp.guarucar.classes.Veiculo;
import br.ifsp.guarucar.classes.CorVeiculo;
import br.ifsp.guarucar.hibernate.HibernateUtil;

@Controller
public class VeiculoController {
	private final Validator validation;

	/**
	 * @deprecated CDI eyes only
	 */

	protected VeiculoController() {
		this(null);
	}

	@Inject
	public VeiculoController(Validator validation) {
		this.validation = validation;
	}

	@Inject
	private Result result;

	public void novo() {
		// função respondavel pelo redirecionamento para a tela de cadastro
		//pesquisa de cores
		CorVeiculo cor = new CorVeiculo();
		List<?> resultado = HibernateUtil.buscar(cor, "nome");
		result.include("resultado",resultado);
	}

	public void salvar(Veiculo veiculo) throws ParseException {
		veiculo.setEstado(1);
		// validação de dados
		if (veiculo.getDescricao() == "")
			validation.add(new SimpleMessage("erro",
					"Não se esquece de adicionar uma descrição, assim fica mais simples alguem se interessar"));
		else if (veiculo.getModelo() == "")
			validation.add(new SimpleMessage("erro", "Não se esquece de adicionar um modelo"));

		// erro
		validation.onErrorRedirectTo(this).novo();
		// salvar
		HibernateUtil.salvar(veiculo);// funcao responsavel para salvar os dados
										// do veiculo no banco
		// sucesso
		result.include("mensagem", "Veiculo cadastrado com sucesso ! O pessoal vai gostar de alugar esse carro !");
		result.redirectTo(this).novo();// redireciona

	}

	public void todos() {
		// função que exibe todos os veiculos cadastrados na base de dados
		Veiculo veiculo = new Veiculo();
		List<?> resultado = HibernateUtil.buscar(veiculo, "categoria");
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Não existem veículos cadastrados, vamos cadastrar um novo"));
		validation.onErrorRedirectTo(this).novo();
		result.include("resultado", resultado);
	}
	
	public void cor() {
		// função que abre formuçário para cadastro de cores
		CorVeiculo corVeiculo = new CorVeiculo();
		List<?> resultado = HibernateUtil.buscar(corVeiculo, "id");
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Não existem cores cadastradas"));
		validation.onErrorRedirectTo(this).cor();
		result.include("resultado", resultado);
	}
	
	public void salvarCor(CorVeiculo corVeiculo) throws ParseException {
		corVeiculo.setStatus(1);
		// validação de dados
		if (corVeiculo.getNome() == "")
			validation.add(new SimpleMessage("erro",
					"coloca a cor do carango"));
		
		// erro
		validation.onErrorRedirectTo(this).cor();
		// salvar
		HibernateUtil.salvar(corVeiculo);// funcao responsavel para salvar os dados
										// do veiculo no banco
		// sucesso
		result.include("mensagem", "Cor cadastrada com sucesso !");
		result.redirectTo(this).cor();// redireciona

	}
	
	public void editar(Veiculo veiculo) {
		Veiculo padrao = (Veiculo) HibernateUtil.pesquisaObjeto("idVeiculo", veiculo.getIdVeiculo(), veiculo);
		padrao.setEstado(veiculo.getEstado());
		HibernateUtil.alterar(padrao);
		result.include("mensagem", "Situação do veiculo alterada com sucesso !");
		result.redirectTo(this).todos();// redireciona
	}
}
