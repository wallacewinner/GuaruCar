package br.ifsp.guarucar.controllers;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.ifsp.guarucar.classes.Usuario;
import br.ifsp.guarucar.hibernate.HibernateUtil;

@Controller
public class UsuarioController {
	private final Validator validation;

	/**
	 * @deprecated CDI eyes only
	 */

	protected UsuarioController() {
		this(null);
	}

	@Inject
	public UsuarioController(Validator validation) {
		this.validation = validation;
	}

	@Inject
	private Result result;
	
	
	
	public void novo() {
		// função respondavel pelo redirecionamento para a tela de cadastro
	}

	public void salvar(Usuario usuario) {

		// erro
		validation.onErrorRedirectTo(this).novo();

		// salvar
		HibernateUtil.salvar(usuario);

		// sucesso
		result.include("mensagem", "Usuário cadastrado com sucesso !");
		result.redirectTo(AluguelController.class).inicio(null, null);// redireciona

	}
}