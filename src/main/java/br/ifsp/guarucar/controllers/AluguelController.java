package br.ifsp.guarucar.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.validator.SimpleMessage;
import br.com.caelum.vraptor.validator.Validator;
import br.ifsp.guarucar.classes.Aluguel;
import br.ifsp.guarucar.classes.CorVeiculo;
import br.ifsp.guarucar.classes.Ocorrencia;
import br.ifsp.guarucar.classes.Sessao;
import br.ifsp.guarucar.classes.Taxa;
import br.ifsp.guarucar.classes.Veiculo;
import br.ifsp.guarucar.hibernate.HibernateUtil;

@Controller
public class AluguelController {
	private final Validator validation;
	private final Sessao sessao;
	/**
	 * @deprecated CDI eyes only
	 */

	protected AluguelController() {
		this(null,null);
	}

	@Inject
	public AluguelController(Validator validation, Sessao sessao) {
		this.validation = validation;
		this.sessao = sessao;
	}

	@Inject
	private Result result;

	public void novo(Veiculo veiculo) {
		List<?> resultado = HibernateUtil.pesquisaIntUnico("idVeiculo", veiculo.getIdVeiculo(), veiculo);
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "ERRO"));
		validation.onErrorRedirectTo(this).inicio(null,null);
		result.include("resultado", resultado);
	}

	public void salvar(Aluguel aluguel) throws ParseException {
		aluguel.setStatusEntrega(0);
		aluguel.setStatusRetirada(0);
		//salvar cliente da sessao atual
		aluguel.setIdCliente(sessao.getIdLogin());
		
		// erro
		validation.onErrorRedirectTo(this).inicio(null,null);
		// salvar
		HibernateUtil.salvar(aluguel);// funcao responsavel para salvar os dados
	
		result.include("mensagem", "Aluguel cadastrado com sucesso!");
		result.redirectTo(this).inicio(null,null);// redireciona

	}

	public void todos(String campo, int valor) {
		// função que exibe todos os veiculos cadastrados na base de dados
		Aluguel aluguel = new Aluguel();

		if(valor == -1){
			List<?> resultado = HibernateUtil.buscar(aluguel, "idCliente");
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Não existem veículos cadastrados, vamos cadastrar um novo"));
		validation.onErrorRedirectTo(this).todos(null,-1);
		result.include("resultado", resultado);	
		} else {
		List<?> resultado = HibernateUtil.buscarAluguel(valor);	
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Sua busca não encontrou nenhum aluguel!"));
		validation.onErrorRedirectTo(this).todos(null,-1);	
		result.include("resultado", resultado);			
		}
		
	}
	
	public void editar(Aluguel aluguel) {
		Aluguel padrao = (Aluguel) HibernateUtil.pesquisaObjeto("idAluguel", aluguel.getIdAluguel(), aluguel);
		padrao.setStatusEntrega(aluguel.getStatusEntrega());
		padrao.setStatusRetirada(aluguel.getStatusRetirada());
		HibernateUtil.alterar(padrao);
		result.include("mensagem", "Situação do Aluguel alterada com sucesso !");
		result.redirectTo(this).todos(null,-1);// redireciona
	}
	
	public void ocorrencia(Aluguel aluguel) {
		// função respondavel pelo redirecionamento para a tela de cadastro
		List<?> resultado = HibernateUtil.pesquisaIntUnico("idAluguel",aluguel.getIdAluguel(), aluguel);
		result.include("resultado", resultado );
	}
	
	public void salvarOcorrencia(Aluguel aluguel, Ocorrencia ocorrencia) throws ParseException {
		Date data = new Date();
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		ocorrencia.setData(formatador.format( data ));
		ocorrencia.setIdAluguel(aluguel.getIdAluguel());
		ocorrencia.setIdusuario(sessao.getIdLogin());
		ocorrencia.setUsuarioNome(sessao.getNome());
		//salvar cliente da sessao atual
		aluguel.setIdCliente(sessao.getIdLogin());
		
		// erro
		validation.onErrorRedirectTo(this).todos(null,-1);
		// salvar
		HibernateUtil.salvar(ocorrencia);// funcao responsavel para salvar os dados
	
		result.include("mensagem", "Ocorrência cadastrada com sucesso!");
		result.redirectTo(this).todos(null,-1);// redireciona

	}
	
	public void exibirOcorrencia(Aluguel aluguel) {
		// função que exibe todos os veiculos cadastrados na base de dados
		Ocorrencia ocorrencia = new Ocorrencia();
		List<?> resultado = HibernateUtil.pesquisaIntUnico("idAluguel",aluguel.getIdAluguel(), ocorrencia);
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Não existem ocorrencias cadastradas"));
		validation.onErrorRedirectTo(this).todos(null,-1);
		result.include("resultado", resultado);
	}
	
	//taxas
	public void addTaxa(Aluguel aluguel) {
		// função respondavel pelo redirecionamento para a tela de cadastro
		List<?> resultado = HibernateUtil.pesquisaIntUnico("idAluguel",aluguel.getIdAluguel(), aluguel);
		result.include("resultado", resultado );
		//todas as taxas
		Taxa taxa = new Taxa();
		List<?> resultado2 = HibernateUtil.pesquisaIntUnico("idAluguel", aluguel.getIdAluguel(), taxa);
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Não existem taxas cadastradas"));
		validation.onErrorRedirectTo(this).addTaxa(aluguel);
		result.include("resultado2", resultado2);
	}
	
	public void salvarTaxa(Taxa taxa, Aluguel aluguel) throws ParseException {
		taxa.setStatus(1);		
		// erro
		validation.onErrorRedirectTo(this).todos(null,-1);
		// salvar
		HibernateUtil.salvar(taxa);// funcao responsavel para salvar os dados	
		result.include("mensagem", "Taxa cadastrada com sucesso!");
		result.forwardTo(this).addTaxa(aluguel);// redireciona
	}
	
	public void editarTaxa(Taxa taxa, Aluguel aluguel) {
		Taxa padrao = (Taxa) HibernateUtil.pesquisaObjeto("idTaxa", taxa.getIdtaxa(), taxa);
		padrao.setStatus(taxa.getStatus());
		HibernateUtil.alterar(padrao);
		result.include("mensagem", "Situação alterada com sucesso !");
		result.forwardTo(this).addTaxa(aluguel);// redireciona
	}
	
	public void inicio(String campo, String valor) {
		Veiculo veiculo = new Veiculo();
		
		if(campo == null && valor == null){
			List<?> resultado = HibernateUtil.buscar(veiculo, "categoria");
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Não existem veículos cadastrados, vamos cadastrar um novo"));
		validation.onErrorRedirectTo(this).inicio(null,null);
		result.include("resultado", resultado);	
		} else {
		List<?> resultado = HibernateUtil.buscarStringVeiculo(campo, valor);	
		if (resultado == null)
			validation.add(new SimpleMessage("erro", "Sua busca não encontrou nenhum veículo!"));
		validation.onErrorRedirectTo(this).inicio(null,null);	
		result.include("resultado", resultado);			
		}
									
	}
	

	
}
