<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<%@include file='/resources/cabecalho.jsp'%>	
</head>
<body>
	<div class="container" style="text-align:center;">
		
		<h3>Cadastrar Novo Usu�rio</h3>
	
	<form action="salvar" method="Post">
	
		<div class="form-group">
			<span>Login: </span> <input name="usuario.login" required="yes"
				type="text"
				title="O modelo [e algo importante não vamos esquecer !">

			<span>Senha: </span> <input name="" required="yes"
				type="password"
				title="O modelo [e algo importante não vamos esquecer !">
		</div>
		
		<div class="form-group">
			<span>Confirmar senha: </span> <input name="usuario.senha" required="yes"
				type="password"
				title="O modelo [e algo importante não vamos esquecer !">
		</div>
		
		<div class="form-group">
			<span>Nome: </span> <input name="usuario.nome" required="yes"
				type="text"
				title="O modelo [e algo importante não vamos esquecer !">
		</div>

		<div class="form-group">
			<span>Sexo: </span> <select name="usuario.sexo">
				<option value="M">Masculino</option>
				<option value="F">Feminino</option>
			</select>
		</div>
		
		<div class="form-group">
			<span>Data de nascimento: </span> <input name="usuario.dataNascto" required="yes"
				type="date"
				title="O modelo [e algo importante não vamos esquecer !">
		</div>

		<div class="form-group">
			<span>Telefone 1: </span> <input name="usuario.telefone1" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>


		<div class="form-group">
			<span>Telefone 2: </span> <input name="usuario.telefone2" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>

		
		<div class="form-group">
			<span>Cep: </span> <input name="usuario.cep" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Tipo de logradouro: </span> <input name="usuario.tp_logradouro" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Logradouro: </span> <input name="usuario.logradouro" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Numero: </span> <input name="usuario.numero" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Complemento: </span> <input name="usuario.complemento" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Bairro: </span> <input name="usuario.bairro" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Cidade: </span> <input name="usuario.cidade" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		
		
		<div class="form-group">
			<span>Estado: </span> <input name="usuario.estado" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>
		

		<div>
			<input  class="btn btn-default" type="submit" value="Salvar">
		</div>
		
	</form>
	</div>	
</body>
</html>