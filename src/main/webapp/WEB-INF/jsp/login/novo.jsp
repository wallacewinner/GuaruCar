<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<link href="../resources/css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="../resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">	
</head>
<body>
<div class="navbar-header navbar-fixed-top navbar-light" style="background-color: #e3f2fd;"">
      		<a class="navbar-brand" href="/guarucar/">GuaruCar</a>
    	</div>
    	<div>
			<br><br><br><br><h3 class="text-info"><center>Criar novo login</center></h3>
		</div>
	<div class="container">
	<form action="salvar" method="Post" class="form-horizontal">
	
	
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">Login:</label>
			<div class="col-sm-10">
				<input name="login.login" class="form-control" required="yes" type="text" placeholder="Digite seu nome de usu�rio">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">Senha:</label>
			<div class="col-sm-10">
				<input name="" required="yes" class="form-control" type="password" title="O modelo [e algo importante não vamos esquecer !" placeholder="Digite sua senha">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">Confirmar senha:</label>
			<div class="col-sm-10">
				<input name="login.senha" required="yes" class="form-control" type="password" title="O modelo [e algo importante não vamos esquecer !" placeholder="Confirme sua senha">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">CPF:</label>
			<div class="col-sm-10">
				<input name="login.cpf" class="form-control" required="yes" type="text" placeholder="Digite seu n�mero de CPF">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">Email:</label>
			<div class="col-sm-10">
				<input name="login.email" class="form-control" required="yes" type="email" title="O modelo [e algo importante não vamos esquecer !" placeholder="Digite seu email">
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">Usu�rio:</label>
			<div class="col-sm-10">
				<select name="login.idTipoUsuario" class="form-control">
					<option value="0">Selecione o tipo de usu�rio</option>
					<option value="1">Administrador</option>
					<option value="2">Cliente</option>
			</select>
			</div>
		</div>
		
		<div class="form-group">
			<label class="control-label col-sm-2 text-info" for="email">Status:</label>
			<div class="col-sm-10">
				<select name="login.ativo" class="form-control">
					<option value="1">Usu�rio ativo</option>
					<option value="2">Usu�rio inativo</option>
			</select>
			</div>
	</form>
	<div class="form-group">
			<label class="control-label col-sm-2" for="email"></label>
			<input class="btn btn-info form-control" type="submit" value="Salvar">			
		</div>
	</div>	
</body>
</html>