<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>

<title>GuaruCAR - Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link href="../resources/css/style.css" rel="stylesheet" type="text/css" media="all">
<link href="../resources/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
</head>
<body>
	<!--banner start here-->
	<div class="container">
		<div class="banner">
			<div class="header">
				<div class="container">
					<!-- Mensagem de Erro -->
					<c:forEach var="error" items="${errors}">
						<div class="alert alert-danger" role="alert">
							<strong>${error.message}</strong>
						</div>
					</c:forEach>
				</div>
					
				<form action="login" method="Post" class="form-login">
								<h2 id="login-heading">Guarucar</h2>
								<div id="login-holder">

				 					<input id="login-usuario" type="text" name="login.login"  title="N�o esque�a isso � importante" placeholder="Usu�rio" required autofocus/>
									<input id="login-senha" type="password" name="login.senha" style="color: black;" required="yes" title="N�o esque�a isso � importante" placeholder="Senha" required/>

									
									<div id="login-btnEntrar">
										<button class="btn btn-primary btn-block" type="submit">Entrar</button>
									</div>
									
									<div id="login-bottom">
				                        <a  id="esqSenha" href="#"><p>Esqueceu sua senha?<p/></a>
				                        <a  id="cadAqui" href="novo"><p>Cadastre-se Aqui<p/></a>
				                    </div>
								</div>
				</form>
		</div>
	</div>
	</div>
</body>
</html>