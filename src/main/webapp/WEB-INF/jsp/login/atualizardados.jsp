<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>

<title>Guarucar - Atualiza��o de cadastro</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/cabecalho.jsp'%>

<!-- Fun��o para verificar a redigita��o da senha -->
<script type="text/javascript">
	function validaSenha(input) {
		if (input.value != document.getElementById('senha').value) {
			input.setCustomValidity('Repita a senha corretamente');
		} else {
			input.setCustomValidity('');
		}
	}
</script>

</head>
<body>
<div>
			<br><br><br><br><h3 class="text-info"><center>Atualizar dados</center></h3>
		</div>	
				<form action="alterar" method="Post" class="form-horizontal">
					<c:forEach var="resultado" items="${resultado}">

						<div class="form-group">
							<label class="control-label col-sm-2 text-info">Login</label> <input type="text" disabled="disabled" value="${resultado.login}" /> 
								<input name="login.login" type="hidden" value="${resultado.login}" /> 
								<input name="login.idLogin" type="hidden" value="${resultado.idLogin}" /> 
								<input name="login.idTipoUsuario" type="hidden" value="${resultado.idTipoUsuario}" /> 
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2 text-info">CPF</label> <input type="text" name="login.cpf"
								request="yes" value="${resultado.cpf}" />
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2 text-info">E-mail</label> <input type="text" name="login.email"
								request="yes" value="${resultado.email}" />
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2 text-info">Digite sua nova senha</label> <input type="password"
								id="senha" name="login.senha" request="yes" />
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2 text-info">Confirmar sua senha</label> <input type="password"
								request="yes" oninput="validaSenha(this)" />
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2 text-info">Ativo </label> <select name="login.ativo">
								<option value="0">Sim</option>
								<option value="1">N�o</option>
							</select>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-2" for="email"></label>
							<input class="btn btn-info form-control" type="submit" value="Alterar">			
						</div>
					</c:forEach>
				</form>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	</div>
</body>
</html>