<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<%@include file='/resources/cabecalho.jsp'%>
</head>

<body>
	<div class="container">
		<h3>Todos os Alugueis</h3>
		<div align="center">
			<form action="todos">

				<select name="valor" style="width: 100%">

					<option value="0">Em Aberto</option>
					<option value="1">Fechado</option>
					<option value="-1">Todos</option>

				</select> 
				<input type="hidden" name="campo" value="statusEntrega">
				<input type="submit" value="Filtrar" style="width:50%;"/>

			</form>
		</div>
		</br>

		<table class="table">
			<thead>
				<tr>
					<th>Aluguel</th>
					<th>Ciente</th>
					<th>Placa Veiculo</th>

					<th>Endere�o Retirada</th>
					<th>Data Retirada</th>

					<th>Endere�o Entrega</th>
					<th>Data Entrega</th>

					<th>Retirada</th>
					<th>Entrega</th>
					<th>Pedido</th>
					<th>Ocorr�ncia</th>
					<th>Taxas</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="resultado" items="${resultado}">
					<form action="editar">
						<tr>
							<input name="aluguel.idAluguel" type="hidden"
								value="${resultado.idAluguel}">
							<td>${resultado.idAluguel}</td>
							<td>${resultado.idCliente}</td>
							<td>${resultado.placa}</td>

							<td>${resultado.ufRetirada}-${resultado.cidadeRetirada}-${resultado.enderecoRetirada}</td>
							<td>${resultado.dataRetirada}</td>

							<td>${resultado.ufEntrega}-${resultado.cidadeEntrega}-${resultado.enderecoEntrega}</td>
							<td>${resultado.dataEntrega}</td>

							<!-- Status Retirada -->
							<c:choose>
								<c:when
									test="${resultado.statusRetirada eq 0 && resultado.statusEntrega eq 0 }">
									<td><input type="submit" value="AGUARDANDO" /></td>
									<input name="aluguel.statusRetirada" type="hidden" value="1">

									<td><input type="text" value="..." /></td>

									<td><input type="text" value="EM ABERTO" /></td>
								</c:when>

								<c:when
									test="${resultado.statusRetirada eq 1 && resultado.statusEntrega eq 0 }">
									<td><input type="text" disabled="disabled" value="OK" /></td>
									<input name="aluguel.statusRetirada" type="hidden" value="1">

									<td><input type="submit" value="AGUARDANDO" /></td>
									<input name="aluguel.statusEntrega" type="hidden" value="1">

									<td><input type="text" value="EM ABERTO" /></td>

								</c:when>

								<c:when
									test="${resultado.statusRetirada eq 1 && resultado.statusEntrega eq 1 }">
									<td><input type="text" disabled="disabled" value="OK" /></td>
									<td><input type="text" disabled="disabled" value="OK" /></td>
									<td><input type="text" value="FECHADO" /></td>

								</c:when>

								<c:otherwise>
									<td><input type="text" disabled="disabled" value="ERRO" /></td>
									<td><input type="text" disabled="disabled" value="ERRO" /></td>
									<td><input type="text" value="ERRO" /></td>
								</c:otherwise>
							</c:choose>
					</form>
					<form action="ocorrencia">
						<input name="aluguel.idAluguel" type="hidden"
							value="${resultado.idAluguel}">
						<td><input type="submit" style="width: 100%; height: 50%;"
							value="Cadastrar" />
					</form>

					<form action="exibirOcorrencia">
						<input name="aluguel.idAluguel" type="hidden"
							value="${resultado.idAluguel}"> <input type="submit"
							style="width: 100%; height: 50%;" value="Exibir" />
						</td>
					</form>

					<form action="addTaxa">
						<input name="aluguel.idAluguel" type="hidden"
							value="${resultado.idAluguel}">
						<td><input type="submit" style="width: 100%; height: 100%;"
							value="Adicionar/Visualizar" />
						<td>
					</form>
					</tr>

				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>