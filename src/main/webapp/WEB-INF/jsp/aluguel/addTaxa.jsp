<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<%@include file='/resources/cabecalho.jsp'%>
</head>

<body>
	<div class="container">
		<h3>Cadastrar Taxa</h3>
		<form action="salvarTaxa" method="Post">
			<c:forEach var="resultado" items="${resultado}">
				<div class="form-group">
					<input name="aluguel.idAluguel" type="hidden" value="${resultado.idAluguel}">
					<input name="taxa.idAluguel" type="hidden"
						value="${resultado.idAluguel}">
				</div>

				<div class="form-group">
					<span>Nome da Taxa</span> </br> <input name="taxa.nome" type="text"></input>
				</div>

				<div class="form-group">
					<span>Valor</span> </br> <input name="taxa.valor" type="text"></input>
				</div>

				<div class="form-group">
					<input class="btn btn-default" type="submit" value="Salvar">
				</div>
		</form>
		</c:forEach>

		<h3>Todas as Cores</h3>
		<table border="1">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Valor</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
			<c:set var="total" value=""/>
				<c:forEach var="resultado2" items="${resultado2}">
				<c:if test="${resultado2.status eq 1}">
					<c:set var="total" value="${total + resultado2.valor}"/>
				</c:if>				
					<form action="editarTaxa">
						<input name="taxa.idtaxa" type="hidden"	value="${resultado2.idtaxa}">
							<td>${resultado2.nome}</td>
							<td>R$ ${resultado2.valor}</td>
							
						<c:choose>
							<c:when test="${resultado2.status eq 1 }">
								<input name="aluguel.idAluguel" type="hidden" value="${resultado2.idAluguel}">
								<td><input type="submit" style="width: 100%" value="Desativar" /></td>
								<input name="taxa.status" type="hidden" value="0">
							</c:when>
							<c:otherwise>
								<input name="aluguel.idAluguel" type="hidden" value="${resultado2.idAluguel}">
								<input name="taxa.status" type="hidden" value="1">
								<td><input type="submit" style="float: right" style="width: 100%"
									value="Ativar" /></td>
							</c:otherwise>
						</c:choose>
						
						</tr>
					</form>
				</c:forEach>
				
			<tr><th>Total</th>
			<td colspan="2">R$ ${total}</td>
			</tr>
	</div>
</body>
</html>