<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<%@include file='/resources/cabecalho.jsp'%>
</head>

<body>

	<!-- Cadastrar cores -->
	<div class="container">
		<br><br><br><br><h3 class="text-info"><center>Cadastrar cores de ve�culos</center></h3>
		<form action="salvarCor" method="Post" class="form-horizontal">
			<label class="control-label col-sm-2 text-info">Nome da cor</label class="control-label col-sm-2 text-info"> <input type="text" name="corVeiculo.nome"
				required="yes"
				title="Opa n�o esquce da cor do Carango !">
			<input class="btn btn-default" type="submit" value="Salvar">
	</div>
	</form>
	</div>
	<!-- exibir tabela com todas as cores -->
	<div class="container">
		<h3>Todos as Cores</h3>

		<table class="table">
			<thead>
				<tr>
					
					<th>Cor</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="resultado" items="${resultado}">
					<form action="editar">
						<tr>
							<input name="corVeiculo.idcor" type="hidden"
								value="${resultado.idcor}">
							
							<td>${resultado.nome}</td>

						</tr>
					</form>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>