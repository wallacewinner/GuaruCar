<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<%@include file='/resources/cabecalho.jsp'%>	
</head>

<body>
	<div class="container">
	<br><br><br><br><h3 class="text-info"><center>Cadastrar ve�culo</center></h3>
	<form action="salvar" method="Post" class="form-horizontal">

		<div class="form-group">
			<span>Marca</span> <select name="veiculo.marca">
				<option value="ACURA">ACURA</option>
				<option value="AGRALE">AGRALE</option>
				<option value="ALFA ROMEO">ALFA ROMEO</option>
				<option value="AM GEN">AM GEN</option>
				<option value="ASIA MOTORS">ASIA MOTORS</option>
				<option value="ASTON MARTIN">ASTON MARTIN</option>
				<option value="AUDI">AUDI</option>
				<option value="BENTLEY">BENTLEY</option>
				<option value="BMW">BMW</option>
				<option value="BRM">BRM</option>
				<option value="BUGGY">BUGGY</option>
				<option value="BUGRE">BUGRE</option>
				<option value="CADILLAC">CADILLAC</option>
				<option value="CBT JIPE">CBT JIPE</option>
				<option value="CHANA">CHANA</option>
				<option value="CHANGAN">CHANGAN</option>
				<option value="CHERY">CHERY</option>
				<option value="CHEVROLET">CHEVROLET</option>
				<option value="CHRYSLER">CHRYSLER</option>
				<option value="CITROËN">CITROËN</option>
				<option value="CROSS LANDER">CROSS LANDER</option>
				<option value="DAEWOO">DAEWOO</option>
				<option value="DAIHATSU">DAIHATSU</option>
				<option value="DODGE">DODGE</option>
				<option value="DS">DS</option>
				<option value="EFFA MOTORS">EFFA MOTORS</option>
				<option value="ENGESA">ENGESA</option>
				<option value="ENVEMO">ENVEMO</option>
				<option value="FERRARI">FERRARI</option>
				<option value="FIAT">FIAT</option>
				<option value="FIBRAVAN">FIBRAVAN</option>
				<option value="FORD">FORD</option>
				<option value="FOTON">FOTON</option>
				<option value="FYBER">FYBER</option>
				<option value="GEELY">GEELY</option>
				<option value="GREAT WALL">GREAT WALL</option>
				<option value="GURGEL">GURGEL</option>
				<option value="HAFEI">HAFEI</option>
				<option value="HONDA">HONDA</option>
				<option value="HYUNDAI">HYUNDAI</option>
				<option value="ISUZU">ISUZU</option>
				<option value="IVECO">IVECO</option>
				<option value="JAC">JAC</option>
				<option value="JAGUAR">JAGUAR</option>
				<option value="JEEP">JEEP</option>
				<option value="JINBEI">JINBEI</option>
				<option value="JPX">JPX</option>
				<option value="KIA">KIA</option>
				<option value="LADA">LADA</option>
				<option value="LAMBORGHINI">LAMBORGHINI</option>
				<option value="LAND ROVER">LAND ROVER</option>
				<option value="LEXUS">LEXUS</option>
				<option value="LIFAN">LIFAN</option>
				<option value="LOBINI">LOBINI</option>
				<option value="LOTUS">LOTUS</option>
				<option value="MAHINDRA">MAHINDRA</option>
				<option value="MASERATI">MASERATI</option>
				<option value="MATRA">MATRA</option>
				<option value="MAZDA">MAZDA</option>
				<option value="MERCEDES-BENZ">MERCEDES-BENZ</option>
				<option value="MERCURY">MERCURY</option>
				<option value="MG">MG</option>
				<option value="MINI">MINI</option>
				<option value="MITSUBISHI">MITSUBISHI</option>
				<option value="MIURA">MIURA</option>
				<option value="NISSAN">NISSAN</option>
				<option value="PEUGEOT">PEUGEOT</option>
				<option value="PLYMOUTH">PLYMOUTH</option>
				<option value="PONTIAC">PONTIAC</option>
				<option value="PORSCHE">PORSCHE</option>
				<option value="RAM">RAM</option>
				<option value="RELY">RELY</option>
				<option value="RENAULT">RENAULT</option>
				<option value="ROLLS ROYCE">ROLLS ROYCE</option>
				<option value="ROVER">ROVER</option>
				<option value="SAAB">SAAB</option>
				<option value="SATURN">SATURN</option>
				<option value="SEAT">SEAT</option>
				<option value="SHINERAY">SHINERAY</option>
				<option value="SMART">SMART</option>
				<option value="SSANGYONG">SSANGYONG</option>
				<option value="SUBARU">SUBARU</option>
				<option value="SUZUKI">SUZUKI</option>
				<option value="TAC">TAC</option>
				<option value="TOYOTA">TOYOTA</option>
				<option value="TROLLER">TROLLER</option>
				<option value="VOLKSWAGEN">VOLKSWAGEN</option>
				<option value="VOLVO">VOLVO</option>
				<option value="WAKE">WAKE</option>
				<option value="WALK">WALK</option>
			</select>
		</div>

		<div class="form-group">
			<span>Modelo</span> <input name="veiculo.modelo" required="yes"
				type="text"
				title="O modelo [e algo importante não vamos esquecer !">
		</div>

		<div class="form-group">
			<span>Motor</span> <select name="veiculo.motor">
				<option value="1.0">1.0</option>
				<option value="1.4">1.4</option>
				<option value="1.6">1.6</option>
				<option value="2.0">2.0</option>
			</select>
		</div>

		<div class="form-group">
			<span>Qnt. Portas</span> <select name="veiculo.qtdPortas">
				<option value="2">2</option>
				<option value="4">4</option>
				<option value="3">3</option>
				<option value="5">5</option>
			</select>
		</div>

		<div class="form-group">
			<span>Interior</span> <select name="veiculo.interior">
				<option value="Couro Sintetico">Couro Sintetico</option>
				<option value="Couro Natural">Couro Natural</option>
				<option value="Tecido">Tecido</option>
			</select>
		</div>

		<div class="form-group">
			<span>Descri��o</span>
			<textarea rows="15" cols="60" name="veiculo.descricao" required="yes"
				title="Opa aqui que voce vai falar mais sobre o Carango !">
			
			</textarea>
		</div>

		<div class="form-group">
			<span>Cor</span> <select name="veiculo.cor">
			<c:forEach var="resultado" items="${resultado}">
				<option value="${resultado.nome}">${resultado.nome}</option>
			</c:forEach>
			</select>
		</div>

		<div class="form-group">
			<span>Combustiv�l</span> <select name="veiculo.combustivel">
				<option value="Gasolina">Gasolina</option>
				<option value="Biocombustiv�l">Biocombustiv�l</option>
				<option value="Flex">Flex</option>
			</select>
		</div>

		<div class="form-group">
			<span>Placa</span> <input name="veiculo.placa" required="yes"
				type="text" title="Olha nada de esquecer da placa em">
		</div>

		<div class="form-group">
			<span>Cambio</span> <select name="veiculo.cambio">
				<option value="Manual">Manual</option>
				<option value="Automatico">Automatico</option>
			</select>
		</div>

		<div class="form-group">
			<span>Categoria</span> <select name="veiculo.categoria">
				<option value="SUV">SUV</option>
				<option value="Esportivo">Esportivo</option>
				<option value="Conversiv�l">Conversiv�l</option>
				<option value="Hatch">Hatch</option>
				<option value="Sed�">Sed�</option>
			</select>
		</div>


		<div class="form-group">
			<input class="btn btn-default" type="submit" value="Salvar">
		</div>
	</form>
	
	</div>
</body>
</html>