<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@include file='/resources/mensagem.jsp'%>
<%@include file='/resources/cabecalho.jsp'%>
</head>

<body>
	<div class="container">
	<h3>Todos os Veiculos</h3>

	<table class="table">
		<thead>
			<tr>
				<th>Id</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Motor</th>
				<th>Qnt. Portas</th>
				<th>Interior</th>
				<th>Cor</th>
				<th>Combustivel</th>
				<th>Placa</th>
				<th>Cambio</th>
				<th>Categoria</th>
				<th>Estado</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="resultado" items="${resultado}">
				<form action="editar">
					<tr>
						<input name="veiculo.idVeiculo" type="hidden"
							value="${resultado.idVeiculo}">
						<td>${resultado.idVeiculo}</td>
						<td>${resultado.marca}</td>
						<td>${resultado.modelo}</td>
						<td>${resultado.motor}</td>
						<td>${resultado.qtdPortas}</td>
						<td>${resultado.interior}</td>
						<td>${resultado.cor}</td>
						<td>${resultado.combustivel}</td>
						<td>${resultado.placa}</td>
						<td>${resultado.cambio}</td>
						<td>${resultado.categoria}</td>

						<c:choose>
							<c:when test="${resultado.estado eq 1 }">
								<td><input type="submit" value="Desativar" /></td>
								<input name="veiculo.estado" type="hidden" value="0">
							</c:when>
							<c:otherwise>
								<input name="veiculo.estado" type="hidden" value="1">
								<td><input type="submit" style="float: right"
									value="Ativar" /></td>
							</c:otherwise>
						</c:choose>
					</tr>
				</form>
			</c:forEach>
		</tbody>
	</table>
	</div>
</body>
</html>