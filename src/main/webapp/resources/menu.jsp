<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:choose>
	<c:when test="${sessao.tipo eq 1}">
	<div class="navbar-header navbar-fixed-top navbar-light" style="background-color: #e3f2fd;"">
      		<a class="navbar-brand" href="/guarucar/aluguel/inicio">Inicio</a>
			<a class="navbar-brand" href="/guarucar/login/novo">Novo login</a>
			<a class="navbar-brand" href="/guarucar/login/atualizardados">Atualizar login</a>
			<a class="navbar-brand" href="/guarucar/veiculo/novo">Cadastrar ve�culo</a>
			<a class="navbar-brand" href="/guarucar/veiculo/cor">Cadastrar cor de ve�culo</a>
			<a class="navbar-brand" href="/guarucar/veiculo/todos">Exibir ve�culos</a>
			<a class="navbar-brand" href="/guarucar/aluguel/todos">Exibir alugu�is</a>
			<a class="navbar-brand" href="/guarucar/login/logout">Sair</a>      		
    	</div>
	</c:when>

	<c:when test="${sessao.tipo eq 2}">
		<div class="navbar-header navbar-fixed-top navbar-light" style="background-color: #e3f2fd;"">
      		<a class="navbar-brand" href="/guarucar/">GuaruCar</a>
      		<a class="navbar-brand" href="/guarucar/aluguel/inicio">Inicio</a>
			<a class="navbar-brand" href="/guarucar/login/atualizardados">Atualizar login</a>
			<a class="navbar-brand" href="/guarucar/login/logout">Sair</a>      		
    	</div>
		
				
	</c:when>

	<c:otherwise>
		<meta http-equiv="refresh" content="0; url=/guarucar/login/acesso">
	</c:otherwise>

</c:choose>



