create database guarucar;

use guarucar;

create table login(
    idLogin int not null primary key auto_increment,
    cpf char(12) not null unique,
    login varchar(50) not null,
    senha varchar(40) not null,
    email varchar(60) not null,
    ativo bit not null DEFAULT 1,
    idTipoUsuario tinyint not null
    
);

create table usuario(
    idUsuario int not null primary key auto_increment,
    idLogin int not null,
    nome varchar(200) not null,
    sexo char(1) not null,
    dataNascto date not null,
    telefone1 varchar(15) not null,
    telefone2 varchar(15),
    cep varchar(10),
    tp_logradouro char(10),
    logradouro varchar(100),
    numero char(10),
    complemento varchar(60),
    bairro varchar(50),
    cidade varchar(50),
    estado varchar(20),
    foreign key(idLogin) references login(idLogin)


);

create table veiculo(
    idVeiculo int not null primary key auto_increment,
    marca varchar(100) not null,
    modelo varchar(30) not null,
    motor varchar(15) not null,
    qtdPortas int not null,
    interior varchar(10) not null,
    descricao varchar(100) not null,
    cor varchar(30) not null,
    combustivel varchar(15),
    placa varchar(10),
    cambio varchar(15),
    categoria varchar(30)
);
